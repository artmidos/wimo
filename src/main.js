import Vue from 'vue'
import App from './App.vue'
import router from './plugins/Router';
import VueResource from 'vue-resource';
import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

import {
  faPlayCircle,
  faCheckSquare,
  faExclamationTriangle,
  faMapMarkedAlt,
  faTimes,
} from '@fortawesome/free-solid-svg-icons';

library.add(faExclamationTriangle)
library.add(faPlayCircle)
library.add(faCheckSquare)
library.add(faMapMarkedAlt)
library.add(faTimes)


Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.config.productionTip = false
Vue.use(VueResource);

Vue.http.options.root = 'https://api.myjson.com/bins'

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
