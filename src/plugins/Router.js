import Vue from 'vue';
import VueRouter from 'vue-router';
import TasksList from '../views/TasksList.vue';

Vue.use(VueRouter);

const routes = [
    { path: '/', redirect: '/tasks' },
    { path: '/tasks', component: TasksList }
];

export default new VueRouter({
    mode: 'history',
    routes,
})
